from django.db.models.signals import post_save
from django.dispatch import receiver

from admin_site.models import ServiceOrder


@receiver(post_save, sender=ServiceOrder)
def my_handler(sender, **kwargs):
    print('Hello from Signal')