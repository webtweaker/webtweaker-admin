days=(
    ('Sunday','Sunday'),
    ('Monday','Monday'),
    ('Tuesday','Tuesday'),
    ('Wednesday','Wednesday'),
    ('Thursday','Thursday'),
    ('Friday','Friday'),
    ('Saturday','Saturday'),
)

category=(
    ('Auto Repair Shop','Auto Repair Shop'),
    ('Barber Shop/salons','Barber Shop/salons'),
    ('Chiropractic','Chiropractic'),
    ('Dental','Dental'),
    ('Exterminator','Exterminator'),
    ('HVAC','HVAC'),
    ('Mobile Phone Store','Mobile Phone Store'),
    ('Pet Store','Pet Store'),
    ('Real State','Real State'),
    ('Restaurant','Restaurant'),
    ('Veterinary','Veterinary'),
    ('Other Industries','Other Industries'),
)
package=(
    ('Basic','Basic'),
    ('Premium','Premium'),
    ('Business','Business'),
    ('Enterprise','Enterprise'),
)
features=(
    (u'1',u'appointment'),
    (u'2',u'reviews'),
)