from django.contrib import admin

# Register your models here.
from admin_site.models import Package

admin.site.register(Package)