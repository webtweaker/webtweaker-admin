from django.contrib.auth.models import AbstractUser
from django.contrib.postgres.fields import ArrayField
from django.contrib.sites.models import Site, SiteManager
from django.core.exceptions import ImproperlyConfigured
from django.core.validators import RegexValidator, MaxValueValidator, MinValueValidator
from django.db import models

# Create your models here.
from django.db.models import DateField
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.http.request import split_domain_port
from django.views.generic.dates import DayMixin
from notifications.signals import notify

from admin_site.choices import *

class User(AbstractUser):
    is_client=models.BooleanField(default=False)

    def __str__(self):
        return self.username

class Client(models.Model):
    user=models.OneToOneField(User,on_delete=models.CASCADE,primary_key=True)
    is_subscribed=models.BooleanField(default=False)
    address=models.CharField(max_length=500,blank=True)
    business_domain=models.CharField(max_length=60,blank=True,null=True)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone = models.CharField(max_length=15, blank=True, null=True, validators=[phone_regex])
    zip_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    zip_code=models.CharField(max_length=5,blank=True)

class Package(models.Model):
    name=models.CharField(max_length=20,default="Basic")
    price=models.CharField(max_length=20,default="99")

    def __str__(self):
        return self.name

class SiteCredentials(models.Model):
    site = models.OneToOneField(Site,null=True)
    access=models.ManyToManyField(Client)
    package=models.OneToOneField(Package,null=True)
    is_active=models.BooleanField(default=False)


class RepairShop(models.Model):
    name=models.CharField(max_length=60,default="")
    facebook=models.URLField(blank=True)
    instagram=models.URLField(blank=True)
    twitter=models.URLField(blank=True)
    google=models.URLField(blank=True)
    email=models.EmailField(blank=False,null=False)
    phone=models.CharField(max_length=15,blank=False,null=False)
    address=models.CharField(max_length=200)
    site=models.OneToOneField(Site)


class OpenClose(models.Model):
    day = models.CharField(max_length=20, blank=False, null=False, unique=True)
    from_time = models.TimeField(auto_created=True, blank=True, null=True)
    to_time = models.TimeField(auto_created=True, blank=True, null=True)
    closed=models.BooleanField(default=False)

    def __str__(self):
        return self.day
    # shop=models.ForeignKey(RepairShop)

class Review(models.Model):
    name=models.CharField(max_length=60)
    address=models.CharField(max_length=200)
    day=models.DateField(auto_now_add=True)
    message=models.TextField(max_length=2000)
    site=models.ForeignKey(Site)

class ServiceType(models.Model):
    name=models.CharField(max_length=200,null=False,blank=False)
    description=models.CharField(max_length=1000)
    site=models.ForeignKey(Site)

    def __str__(self):
        return self.name

@receiver(post_save,sender=ServiceType)
def appointment_notification(sender,instance,created,**kwargs):
    users=User.objects.all()
    notify.send(instance, verb='was saved',recipient=users)


class ServiceOrder(models.Model):

    APPOINTMENT_TYPES=(
        (1,'Request an Appointment'),
        (2,'Request a Quote')
    )

    name=models.ForeignKey(ServiceType,on_delete=models.CASCADE)
    completed=models.BooleanField(default=False)
    client_name=models.CharField(max_length=200,blank=False,null=False)
    client_mail=models.EmailField(max_length=30,blank=False,null=False)

    phone_regex=RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    client_phone=models.CharField(max_length=15,blank=False,null=False,validators=[phone_regex])

    car_year=models.IntegerField(validators=[MaxValueValidator(2018),
            MinValueValidator(1980)],null=True,blank=True)
    car_make=models.CharField(max_length=100)
    car_model=models.CharField(max_length=255)

    appointment_date=models.DateField()
    message=models.TextField()

    site=models.ForeignKey(Site)

class ContentManager(models.Model):
    pass

class UserPayment(models.Model):
    user=models.ForeignKey(User)
    name=models.CharField(max_length=100,blank=True,null=True)
    address=models.CharField(max_length=100,blank=True,null=True)
    city=models.CharField(max_length=100,blank=True,null=True)
    state=models.CharField(max_length=100,blank=True,null=True)
    token=models.CharField(max_length=500,blank=True,null=True)
    amount=models.PositiveIntegerField()
    date=models.DateTimeField(auto_now_add=True)

class Subscription(models.Model):
    site = models.OneToOneField(Site)
    package = models.ForeignKey(Package)
    start_timestamp = models.DateTimeField(auto_now_add=True)
    end_timestamp = models.DateTimeField()
# @receiver(post_save,sender=UserPayment)
# def appointment_notification(sender,instance,created,**kwargs):
#     user=instance.user
#     notify.send(instance, verb='was saved',recipient=user)
class Permission(models.Model):
    name=models.CharField(max_length=20)