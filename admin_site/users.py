from django.contrib.sites.models import Site

from admin_site.models import User,  Subscription,  UserPayment, Client, SiteCredentials, Package
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, JsonResponse
from admin_site.forms import UsersForm, ProfileForm
import datetime
import stripe
from django.conf import settings

def users_list(request):
    users=User.objects.all()
    page = request.GET.get('page')
    paginator = Paginator(users, 10)

    try:
        users = paginator.page(page)

    except PageNotAnInteger:
        users = paginator.page(1)

    except EmptyPage:
        users = paginator.page(paginator.num_pages)

    return render(request, "admin_site/user_management/users_list.html", {'objects': users})

def user_create(request):
    if request.method=='POST':
        form = UsersForm(request.POST or None)
        if form.is_valid():
            form.save()
            return redirect("admin_site:users_list")
    else:
        form = UsersForm()

    return render(request,"admin_site/user_management/users_create.html",{'form':form})

def user_edit(request,id):
    user = get_object_or_404(User, id=id)
    form = UsersForm(request.POST or None, instance=user)

    if form.is_valid():
        form.save()
        return redirect('admin_site:users_list')

    return render(request, "admin_site/user_management/users_create.html", {'form': form})

def user_delete(request,id):
    user=get_object_or_404(User,id=id)
    user.delete()
    return redirect("admin_site:users_list")

def user_payment_history(request):
    user = request.user
    payments = UserPayment.objects.filter(user=user)

    return render(request, "admin_site/user_management/user_payment_history.html", {'payments' : payments})

def upgrade_payment(request):

    if request.method == 'POST':

        client=Client.objects.get(user=request.user)
        site=Site.objects.filter(sitecredentials__access=client).first()
        duration = int(request.POST.get('duration'))
        amount = int(request.POST.get('amount'))
        package_id = Package.objects.filter(price=amount).first().id
        end_timestamp = datetime.datetime.now()+datetime.timedelta(days=duration*30)
        token = request.POST.get('token')  # Using Flask

        amount = amount *duration * 100

        stripe.api_key = settings.STRIPE_SECRET_KEY
        charge = stripe.Charge.create(
            amount=amount,
            currency='usd',
            description='Basic charge',
            source=token,
        )

        client = Client.objects.get(user=request.user)
        client.is_subscribed = True
        site, site_created = Site.objects.get_or_create(name="demo.com")
        credential, created_cr = SiteCredentials.objects.get_or_create(site=site)
        credential.access.add(client)
        credential.package =  Package.objects.get(pk=package_id)

        client.save()
        credential.save()

        UserPayment.objects.create(user=request.user, name=request.user.username, address=client.address, city='N/A',
                                   state='N/A', token=token,
                                   amount=amount)
        defaults = {
                    'start_timestamp' : datetime.datetime.now(),
                    'package' : Package.objects.get(pk=package_id),
                    'end_timestamp' : end_timestamp
        }
        subcription, created = Subscription.objects.update_or_create(
            site = site,
            defaults=defaults

        )
        return JsonResponse({'charged': 'true'})

        # print(subcription,site,duration)
    else:
        packages = Package.objects.all()
        STRIPE_PUBLISHABLE_KEY=settings.STRIPE_PUBLISHABLE_KEY
        return render(request, "admin_site/profile/payment-upgrade.html", {'packages' : packages,'STRIPE_PUBLISHABLE_KEY':STRIPE_PUBLISHABLE_KEY})