from django.conf.urls import url, include
from django.views.generic.base import TemplateView

from admin_site.dashboard import *
from admin_site.repairshop import *
from admin_site.service import *
from admin_site.users import *
from admin_site.views import *
from admin_site.reviews import *
from admin_site.profile import *
from admin_site.notifications import *

urlpatterns = [
    url(r'^$', view=dashboard, name="dashboard"),
    url(r'notifications/$',view=notifications,name="notifications"),
    url(r'notifications-count/', view=notifications_count, name="notifications_count"),

    #self profile
    url(r'profile/$', view=profile, name="profile"),
    # url(r'update-profile/$', view=update_profile, name="update_profile"),

    # # Service Order Admin Panel

    url(r'user_list/$', view=users_list, name="users_list"),
    url(r'user_list/create/$', view=user_create, name="user_create"),
    url(r'user_list/edit/(?P<id>[0-9]+)/$', view=user_edit, name="user_edit"),
    url(r'user_list/delete/(?P<id>[0-9]+)/$', view=user_delete, name="user_delete"),
    url(r'user_payment_history/$', view=user_payment_history, name="user_payment_history"),
    #
    # # Service Type Admin Panel
    url(r'service-type/$', view=service_type_list, name="service_type_list"),
    url(r'service-type/create/$', view=service_type_create, name="service_type_create"),
    url(r'service-type-edit/(?P<id>[0-9]+)/$', view=service_type_edit, name="service_type_edit"),
    url(r'service-type-delete/(?P<id>[0-9]+)/$', view=service_type_delete, name="service_type_delete"),
    #
    # # Service Order Admin Panel
    url(r'service-order/$', view=service_order_list, name="service_order_list"),
    url(r'service-order/create/$', view=service_order_create, name="service_order_create"),
    url(r'service-order-edit/(?P<id>[0-9]+)/$', view=service_order_edit, name="service_order_edit"),
    url(r'service-order-delete/(?P<id>[0-9]+)/$', view=service_order_delete, name="service_order_delete"),
    #
    # # Repair Shop Admin Panel
    url(r'repair-shop-update/(?P<id>[0-9]+)/$', view=repair_shop_update, name="repair_shop_update"),
    url(r'repair-shop-create/$', view=repair_shop_create, name="repair_shop_create"),
    url(r'repair-shop/$', view=repair_shop_list, name="repair_shop_list"),
    url(r'repair-shop-delete/(?P<id>[0-9]+)/$', view=repair_shop_delete, name="repair_shop_delete"),
    #
    #
    url(r'repair-shop-days/$', view=repair_shop_days, name="repair_shop_days"),
    url(r'timing_create/$', view=timing_create, name="timing_create"),
    # url(r'timing_update/(?P<id>[0-9]+)/$', view=timing_update, name="timing_update"),
    # url(r'timing_delete/(?P<id>[0-9]+)/$', view=timing_delete, name="timing_delete"),

    #
    # #SEO Optimization
    # url(r'optimizations/',name="optimizations_list"),
    # url(r'optimizations/create/',name="optimizations_create"),
    # url(r'optimizations/edit/(?P<id>[0-9]+)/',name="optimizations_edit"),
    # url(r'optimizations/delete/(?P<id>[0-9]+)/',name="optimizations_delete"),

    #
    #
    # # Review Admin Panel
    url(r'review/$', view=review_list, name="review_list"),
    url(r'review/create/$', view=review_create, name="review_create"),
    url(r'review-edit/(?P<id>[0-9]+)/$', view=review_edit, name="review_edit"),
    url(r'review-delete/(?P<id>[0-9]+)/$', view=review_delete, name="review_delete"),

    #
    #
    ## Upgrade Payment
    url(r'upgrade-payment', view = upgrade_payment, name='upgrade-payment')
]
