from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.sites.models import Site

from django.core.validators import RegexValidator
from django.forms import ModelForm, Select
#
from admin_site.choices import days, category
from admin_site.models import RepairShop, OpenClose, Client, ServiceOrder
from admin_site.models import ServiceType
from admin_site.models import Review
#
class ServiceTypeForm(forms.ModelForm):
    sites=Site.objects.all()
    site=forms.ModelChoiceField(queryset=sites,widget=forms.Select(attrs={'class':'mdb-select'}))
    class Meta:
        model=ServiceType
        fields=['name','description','site']
        widgets = {
            'name':forms.TextInput(attrs={'class':'form-control'}),
            'description':forms.TextInput(attrs={'class':'form-control'})
        }
class ServiceOrderForm(forms.ModelForm):
    services=ServiceType.objects.all()
    sites = Site.objects.all()
    name=forms.ModelChoiceField(queryset=services,widget=forms.Select(attrs={'class':'mdb-select'}))
    site=forms.ModelChoiceField(queryset=sites,widget=forms.Select(attrs={'class':'mdb-select'}))
    class Meta:
        model=ServiceOrder
        fields=['client_name','client_mail','client_phone','car_year','car_make','car_model','appointment_date','message','name','site']
        widgets = {
            'client_name':forms.TextInput(attrs={'class':'form-control'}),
            'client_mail':forms.TextInput(attrs={'class':'form-control'}),
            'client_phone':forms.TextInput(attrs={'class':'form-control'}),
            'car_year':forms.TextInput(attrs={'class':'form-control'}),
            'car_make':forms.TextInput(attrs={'class':'form-control'}),
            'car_model':forms.TextInput(attrs={'class':'form-control'}),
            'appointment_date':forms.TextInput(attrs={'class':'form-control'}),
            'message':forms.TextInput(attrs={'class':'form-control'})
        }

class UsersForm(forms.ModelForm):
    password=forms.CharField(widget=forms.PasswordInput())
    confirm_password=forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model=get_user_model()
        fields=['username','email','password','confirm_password','first_name','last_name']
        labels={
            'is_active':'Admin',
            'is_superuser':'Super Admin'
        }

    def __init__(self,*args,**kwargs):
        super(UsersForm,self).__init__(*args,**kwargs)
        self.fields['email'].required = True

    def clean(self):
        cleaned_data=super(UsersForm,self).clean()
        password=cleaned_data.get("password")
        confirm_password=cleaned_data.get("confirm_password")

        if password!=confirm_password:
            raise forms.ValidationError(
                "Password and confirm password does not match"
            )

class ProfileForm(forms.ModelForm):

    class Meta:
        model=User
        fields=['username','first_name','last_name']

class ShopOpenCloseForm(forms.ModelForm):
    day=forms.ChoiceField(choices=days,widget=forms.Select(attrs={'class':'mdb-select'}))

    class Meta:
        model=OpenClose
        fields=['day','from_time','to_time','closed']
        widgets={
            'from_time':forms.TimeInput(attrs={'class':'timepicker form-control'}),
            'to_time':forms.TimeInput(attrs={'class':'timepicker form-control'}),
        }
        labels={
            'from_time':'From',
            'to_time':'To'
        }

class RepairShopUpdateForm(forms.ModelForm):
    sites=Site.objects.all()
    site=forms.ModelChoiceField(queryset=sites,widget=forms.Select(attrs={'class':'mdb-select'}))
    class Meta:
        model=RepairShop
        fields=['name','email','address','phone','facebook','instagram','twitter','google','site']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'email': forms.TextInput(attrs={'class': 'form-control'}),
            'address': forms.TextInput(attrs={'class': 'form-control'}),
            'phone': forms.TextInput(attrs={'class': 'form-control'}),
            'facebook': forms.TextInput(attrs={'class': 'form-control'}),
            'instagram': forms.TextInput(attrs={'class': 'form-control'}),
            'twitter': forms.TextInput(attrs={'class': 'form-control'}),
            'google': forms.TextInput(attrs={'class': 'form-control'})
        }



class SignupForm(UserCreationForm):
    address=forms.CharField(max_length=200)
    zip_code=forms.IntegerField(max_value=9999,min_value=1000)
    business_domain=forms.ChoiceField(choices=category,widget=forms.Select())
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone_no =forms.CharField(max_length=15,validators=[phone_regex])
    about=forms.CharField(widget=forms.Textarea())
    class Meta:
        model=get_user_model()
        fields=['username','email','address','zip_code','password1','password2','business_domain','phone_no']
        # fields=['username','email','password1','password2','phone_no']

    def signup(self, request, user):
        # user.username = self.cleaned_data['username']
        # user.email = self.cleaned_data['email']

        user=user.save()

class ReviewForm(forms.ModelForm):
    sites = Site.objects.all()
    site = forms.ModelChoiceField(queryset=sites, widget=forms.Select(attrs={'class': 'mdb-select'}))

    class Meta:
        model = Review
        fields = ['name','address','message','site']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'address': forms.TextInput(attrs={'class': 'form-control'}),
            'message': forms.TextInput(attrs={'class': 'form-control'}),
        }
