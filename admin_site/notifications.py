from django.http import JsonResponse
from notifications.models import Notification


def notifications_count(request):
    data = {}
    data['noti_count'] = Notification.objects.unread().filter(recipient=request.user).count()
    return JsonResponse(data)