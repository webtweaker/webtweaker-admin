from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from admin_site.models import Client, SiteCredentials, UserPayment


@login_required
def dashboard(request):
    if request.user.is_client==True:
        if request.user.client.is_subscribed==True :
            user=request.user
            data={}
            client=Client.objects.get(user=user)
            sitec=SiteCredentials.objects.filter(access=client).first()
            data['site_name']=sitec.site.name
            data['site_domain']=sitec.site.domain
            data['end_subscription']=sitec.site.subscription.end_timestamp
            data['package']=sitec.package.name
            data['package_price']=sitec.package.price
            payment=UserPayment.objects.filter(user=request.user).first()
            data['last_payment']=payment.date
            data['last_payment_amount']=payment.amount
            data['is_subscribed'] =True
        else:
            data = {}
            data['is_subscribed']=False


        return render(request,"admin_site/dashboard/dashboard.html",data)
    else:
        return render(request, "admin_site/dashboard/dashboard.html", {})