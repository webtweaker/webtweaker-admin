import stripe
from django.contrib.auth.decorators import login_required
from django.contrib.sites.models import Site
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect


# Create your views here.
from admin_site.models import UserPayment, Client, SiteCredentials, Package


def homepage(request):
    return render(request,"mainsite/index.html",{})

def redirection(request):
    if request.user.client.is_subscribed == False:
        return redirect('admin_site:dashboard')

@login_required
def payment(request):
    return render(request,"mainsite/payment.html",{})

def subscribe_redirect(request):
    if request.user:
        return redirect('/account/login/')
    else:
        return redirect('/account/signup/')

def payment_stripe(request):
    name=request.POST.get('name')
    address=request.POST.get('address')
    city=request.POST.get('city')
    state=request.POST.get('state')
    amount=request.POST.get('amount')
    package=request.POST.get('package')

    package=Package.objects.get(name=package)

    amount=int(amount)*100
    stripe.api_key = "sk_test_zqnEKz2EPipWOAfzlYXPjnCe"
    token = request.POST.get('token')  # Using Flask
    UserPayment.objects.create(user=request.user,name=name,address=address,city=city,state=state,token=token,amount=amount)
    charge = stripe.Charge.create(
        amount=amount,
        currency='usd',
        description='Basic charge',
        source=token,
    )
    client=Client.objects.get(user=request.user)
    client.is_subscribed=True
    site,site_created = Site.objects.get_or_create(name="demo.com")
    credential,created_cr=SiteCredentials.objects.get_or_create(site=site)
    credential.access.add(client)
    credential.package=package

    client.save()
    credential.save()
    return JsonResponse({'charged':'true'})

def portfolio_repair_shop(request):
    return render(request,"mainsite/portfolio/repair-shop.html",{})