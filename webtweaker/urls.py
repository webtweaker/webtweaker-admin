from __future__ import absolute_import, print_function, unicode_literals

from allauth.socialaccount.providers import paypal
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.static import serve
from django.conf.urls import url, include
from django.contrib import admin

admin.autodiscover()


urlpatterns = [
    url(r'^admin/', include('admin_site.urls',namespace="admin_site")),
    url(r'^', include('mainsite.urls',namespace="mainsite")),
    url(r'^account/', include('allauth.urls')),
    url(r'^admin-bk/', admin.site.urls),
    # url(r'^$',view=home,name="home"),
    #
    #
    #
    # url(r'^services/$',view=services,name="services"),
    #
    # url(r'appointment/create/$',view=appointment_create,name="appointment_create"),
    #
    # url(r'^about-us/$', view=about_us, name="about_us"),
    # url(r'^gallery/$', view=gallery, name="gallery"),
    # url(r'^contact-us/$', view=contact_us, name="contact_us"),
    # url(r'^payments/$', view=payments, name="payments"),

    url('^inbox/notifications/', include('notifications.urls', namespace='notifications')),
]
